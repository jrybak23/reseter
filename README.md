### Ho to run:
#### Using curl
```bash
curl https://gitlab.com/jrybak23/reseter/-/raw/master/src/main/java/com/example/App.java?inline=false --output App.java
java App.java
```
#### Using wget
```bash
wget https://gitlab.com/jrybak23/reseter/-/raw/master/src/main/java/com/example/App.java?inline=false --output App.java
java App.java
```