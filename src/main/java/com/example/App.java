package com.example;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static com.example.App.OSUtil.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Base64.getDecoder;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;


public class App {
    private static final String MAGIC_STRING_PASCAL_CASE = base64Decode("SmV0QnJhaW5z");
    private static final String MAGIC_STRING_LOWERCASE = base64Decode("amV0YnJhaW5z");

    private static String base64Decode(String input) {
        byte[] bytes = getDecoder().decode(input);
        return new String(bytes, UTF_8);
    }

    public static void main(String[] args) {
        new App().run();
    }

    private void run() {
        System.out.println("Deleting preferences API node..");
        deletePreferencesApiNode();
        String pathToDelete = getPathToDelete();
        System.out.println("Deleting the config dir " + pathToDelete);
        deleteDir(pathToDelete);
        showMessageDialog(null, "Reset successfully");
    }

    private void deletePreferencesApiNode() {
        try {
            Preferences.userRoot().node(MAGIC_STRING_LOWERCASE).removeNode();
        } catch (BackingStoreException e) {
            showErrorMessage(e);
            throw new RuntimeException(e);
        }
    }

    private String getPathToDelete() {
        String userHomePath = System.getProperty("user.home");

        String pathToDelete;
        if (isWindows()) {
            pathToDelete = userHomePath + "\\AppData\\Roaming\\" + MAGIC_STRING_PASCAL_CASE;
        } else if (isLinux()) {
            pathToDelete = userHomePath + "/.config/" + MAGIC_STRING_PASCAL_CASE;
        } else if (isOSX()) {
            pathToDelete = userHomePath + "/.config/" + MAGIC_STRING_PASCAL_CASE;
        } else {
            throw new RuntimeException("Unsupported OS " + OSUtil.OS_NAME);
        }
        return pathToDelete;
    }

    private void deleteDir(String pathToDelete) {
        try {
            Files.walk(Paths.get(pathToDelete))
                    .map(Path::toFile)
                    .sorted((o1, o2) -> -o1.compareTo(o2))
                    .forEach(File::delete);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void showErrorMessage(BackingStoreException e) {
        String stackTrace = getStackTrace(e);
        showMessageDialog(null, stackTrace, "Error", ERROR_MESSAGE);
    }

    private String getStackTrace(BackingStoreException e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    public static class OSUtil {

        public static final String OS_NAME = System.getProperty("os.name");

        public static boolean isWindows() {
            return osNameStartWith("Windows");
        }

        public static boolean isLinux() {
            return osNameStartWith("LINUX") || osNameStartWith("Linux");
        }

        public static boolean isOSX() {
            return osNameStartWith("Mac");
        }

        private static boolean osNameStartWith(String prefix) {
            return OS_NAME != null && OS_NAME.startsWith(prefix);
        }
    }
}
